#version 450 core

precision mediump int; precision highp float;

vec3 getLo_PbrIlluminationCore(in vec3 n,in vec3 light_dir,in vec3 to_camera,in vec3 light_color);
vec3 getAmbient_PbrIlluminationCore(in vec3 light_ambient_sum);

struct DirLight_LampSignatures_PBR { vec3 direction; vec3 ambient; vec3 color; };
struct SpotLight_LampSignatures_PBR { vec3 pos; vec4 direction; vec4 attenuation; vec3 ambient; vec3 color; };
struct DotLight_LampSignatures_PBR { vec3 pos; vec3 attenuation; vec3 ambient; vec3 color; };

int getDirLightCount_LampSignatures_PBR();
int getSpotLightCount_LampSignatures_PBR();
int getDotLightCount_LampSignatures_PBR();

DirLight_LampSignatures_PBR getDirLight_LampSignatures_PBR(in int i);
SpotLight_LampSignatures_PBR getSpotLight_LampSignatures_PBR(in int i);
DotLight_LampSignatures_PBR getDotLight_LampSignatures_PBR(in int i);


layout(std140, binding = 1, set = 0) uniform SIB_CAMERA
{vec4 camPos;
    mat4 view_mat;
    mat4 viewInverted;
    mat3 normal;
} camera;

vec3 get_illumination_PBR_illumination(vec3 normal_frag, vec3 fragPosWorld)
{
  vec3 Lo = vec3(0.0);
  vec3 ambient = vec3(0.0);
  vec3 toCamera = normalize(camera.camPos.xyz - fragPosWorld);
  vec3 N = normalize(normal_frag);

  for(int i = 0; i < getDirLightCount_LampSignatures_PBR(); ++i)
  {
    ambient += getDirLight_LampSignatures_PBR(i).ambient;
    vec3 lightDir = -normalize(getDirLight_LampSignatures_PBR(i).direction);
    Lo += getLo_PbrIlluminationCore(N, lightDir, toCamera, getDirLight_LampSignatures_PBR(i).color);
  }

  for(int i = 0; i < getSpotLightCount_LampSignatures_PBR(); ++i)
  {
    vec3 lightDir = getSpotLight_LampSignatures_PBR(i).pos - fragPosWorld;
    float distance = length(lightDir);

    lightDir /= distance;

    vec4 attenuation = getSpotLight_LampSignatures_PBR(i).attenuation;
    attenuation.zy *= distance;
    attenuation.z  *= distance;
    float att = 1.0 / ( attenuation.z + attenuation.y + attenuation.x );

    float spot_cutoff = cos(attenuation.w);
    vec4 spot = getSpotLight_LampSignatures_PBR(i).direction;
    float spot_exponent = spot.w;

    vec3 spot_direction = spot.xyz;
    float spot_effect = dot(normalize(spot_direction), -lightDir);
    if(spot_effect < spot_cutoff)
      continue;
    spot_effect = max(pow(spot_effect, spot_exponent), 0.0);
    att *= spot_effect;

    ambient += att * getSpotLight_LampSignatures_PBR(i).ambient;
    Lo += att * getLo_PbrIlluminationCore(N, lightDir, toCamera, getSpotLight_LampSignatures_PBR(i).color);
  }

  for(int i = 0; i < getDotLightCount_LampSignatures_PBR(); ++i)
  {
    vec3 lightDir = getDotLight_LampSignatures_PBR(i).pos - fragPosWorld;
    float distance = length(lightDir);

    lightDir /= distance;

    vec3 attenuation = getDotLight_LampSignatures_PBR(i).attenuation;
    attenuation.zy *= distance;
    attenuation.z  *= distance;
    float att = 1.0 / ( attenuation.z + attenuation.y + attenuation.x );

    ambient += att * getDotLight_LampSignatures_PBR(i).ambient;
    Lo += att * getLo_PbrIlluminationCore(N, lightDir, toCamera, getDotLight_LampSignatures_PBR(i).color);
  }

  return Lo + getAmbient_PbrIlluminationCore(ambient);
}



