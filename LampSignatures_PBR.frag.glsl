#version 450 core

precision mediump int; precision highp float;

struct DirLight_LampSignatures_PBR
{
  vec3 direction;
vec3 ambient;
vec3 color;

};

struct SpotLight_LampSignatures_PBR
{
  vec3 pos;
vec4 direction;
vec4 attenuation;
vec3 ambient;
vec3 color;

};

struct DotLight_LampSignatures_PBR
{
  vec3 pos;
vec3 attenuation;
vec3 ambient;
vec3 color;

};

# if (1 > 0 || 0 > 0 || 0 > 0)

layout(std140, binding = 0, set = 4) uniform globalUboIllumination
{

# if (1 > 0)
  DirLight_LampSignatures_PBR dirLights[1];
# endif
# if (0 > 0)
  SpotLight_LampSignatures_PBR spotLights[0];
# endif
# if (0 > 0)
  DotLight_LampSignatures_PBR dotLights[0];
# endif
} PBR_lights;

# endif


int getDirLightCount_LampSignatures_PBR() { return 1; }
int getSpotLightCount_LampSignatures_PBR() { return 0; }
int getDotLightCount_LampSignatures_PBR() { return 0; }

DirLight_LampSignatures_PBR getDirLight_LampSignatures_PBR(in int i)
{
#if (1 > 0)
    return PBR_lights.dirLights[i];
#else
    DirLight_LampSignatures_PBR result;
    return result;
#endif
}

SpotLight_LampSignatures_PBR getSpotLight_LampSignatures_PBR(in int i)
{
#if (0 > 0)
    return PBR_lights.spotLights[i];
#else
    SpotLight_LampSignatures_PBR result;
    return result;
#endif
}

DotLight_LampSignatures_PBR getDotLight_LampSignatures_PBR(in int i)
{
#if (0 > 0)
    return PBR_lights.dotLights[i];
#else
    DotLight_LampSignatures_PBR result;
    return result;
#endif
}




