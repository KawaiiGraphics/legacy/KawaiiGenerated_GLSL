#version 450 core


layout(location = 0) in vec2 texCoord;
layout(location = 1) in vec3 norm_vec;
layout(location = 2) in vec3 pos_vec;



layout(binding = 8, set = 1) uniform sampler2D tex;

vec3 get_illumination_PBR_illumination(vec3 normal_frag,vec3 fragPosWorld);

struct MaterialInfo_PbrIlluminationCore { vec3 albedo; float metallic; float roughness; float ao; };


//albedo; metallic; roughness; ao;
MaterialInfo_PbrIlluminationCore pbr_material;

layout(std140, binding = 9, set = 4) uniform globalUboGamma
{
    float exposure;
    float gamma;
} gamma;

layout(location = 0) out vec4 result;

void main()
{
    pbr_material.metallic = 0.05;
    pbr_material.roughness = 0.5;
    pbr_material.ao = 1.0;

    vec4 texel = texture(tex, texCoord);
    if(texel.a <= 0.001)
    {
        discard;
        return;
    }
    pbr_material.albedo = pow(texel.rgb, vec3(2.2));
    vec3 c = get_illumination_PBR_illumination(norm_vec, pos_vec);
    vec3 mapped = vec3(1.0) - exp(-c * gamma.exposure);
    mapped = pow(mapped, vec3(1.0 / gamma.gamma));
    result = vec4(mapped, texel.a);
}
